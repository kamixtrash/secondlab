import model.Client.Recipient;
import model.Client.Sender;
import model.Currency.Currency;
import model.Transaction.AccountToAccountTransaction;
import model.Transaction.CardToCardTransaction;
import model.Transaction.Transaction;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Класс с тестами для проверки работоспособности приложения.
 */
public class TransactionTest {

    static Recipient client1;
    static Sender client2;
    static Sender client3;

    @BeforeClass
    public static void prepareData() {
        client1 = new Recipient("Ivanov", "Ivan", "Ivanovich", "RGNJ-1235-8452-RNJF", 345674, 1000L);
        client2 = new Sender("Petrov", "Petr", "Petrovich", "JTDS-2378-3412-OKAN", 124663, 2000L);
        client3 = new Sender("Nikolaev", "Nikolay", "Nikolayevich", "1634-SARJ-KIGK-5667", 127867, -100L);
    }

    /**
     * Валидация перевода клиента, баланс которого ниже нуля.
     *
     */
    @Test
    public void transactionCheck_balanceBelowZero() {
        Transaction transaction = new CardToCardTransaction(100L, Currency.RUB, client1, client3);
        assertFalse(transaction.check());
    }

    /**
     * Валидация перевода, где сумма превышает количество средств на балансе клиента.
     *
     */
    @Test
    public void transactionCheck_amountAboveBalance() {
        Transaction transaction = new CardToCardTransaction(5000L, Currency.USD, client1, client2);
        assertFalse(transaction.check());
    }

    /**
     * Успешный перевод, в котором соблюдены все условия.
     *
     */
    @Test
    public void transactionCommit_success() {
        Transaction transaction = new AccountToAccountTransaction(100L, Currency.RUB, client1, client2);
        transaction.commit();
        assertTrue(client1.getBalance() == 1100L && client2.getBalance() == 1900L);
    }

    /**
     * Отменённый перевод. У клиента номер 1 не хватает средств.
     */
    @Test
    public void transactionCommit_fail() {
        Transaction transaction = new AccountToAccountTransaction(5000L, Currency.EUR, client1, client2);
        assertFalse(transaction.check());
    }
}
