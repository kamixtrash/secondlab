import model.Client.Recipient;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClientTest {

    static Recipient client1;

    @BeforeClass
    public static void prepareData() {
        client1 = new Recipient("Ivanov", "Ivan", "Ivanovich", "RGNJ-1235-8452-RNJF", 345674, 1000L);
    }

    @Test
    public void ClientGetFullName_Normal() {
        assertEquals("Ivanov Ivan Ivanovich", client1.toString());
    }

    @Test
    public void ClientGetFullName_Upper() {
        assertEquals("IVANOV IVAN IVANOVICH", client1.toStringUpper());
    }

    @Test
    public void ClientGetFullName_Lower() {
        assertEquals("ivanov ivan ivanovich", client1.toStringLower());
    }
}
