package model.Client;

import java.util.Objects;

/**
 * Клиент. Может быть как переводящим так и получающим средства лицом.
 */
public class Client {
    private String lastName;
    private String firstName;
    private String patronymic;
    private String cardNumber;
    private Integer accountNumber;
    private Long balance;

    /**
     * Полный конструктор для клиента.
     *
     * @param lastName      фамилия клиента.
     * @param firstName     имя клиента.
     * @param patronymic    отчество клиента.
     * @param cardNumber    номер карты клиента.
     * @param accountNumber номер счёта клиента.
     * @param balance       общий баланс.
     */
    public Client(String lastName, String firstName, String patronymic, String cardNumber, Integer accountNumber, Long balance) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.balance = balance;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Integer accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(lastName, client.lastName) && Objects.equals(firstName, client.firstName) && Objects.equals(patronymic, client.patronymic) && Objects.equals(cardNumber, client.cardNumber) && Objects.equals(accountNumber, client.accountNumber) && Objects.equals(balance, client.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, patronymic, cardNumber, accountNumber, balance);
    }

    @Override
    public String toString() {
        return lastName + " " + firstName + " " + patronymic;
    }

    public String toStringLower() {
        return toString().toLowerCase();
    }

    public String toStringUpper() {
        return toString().toUpperCase();
    }
}