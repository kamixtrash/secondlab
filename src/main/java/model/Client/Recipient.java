package model.Client;

public class Recipient extends Client {
    /**
     * Полный конструктор для получателя.
     *
     * @param lastName      фамилия клиента.
     * @param firstName     имя клиента.
     * @param patronymic    отчество клиента.
     * @param cardNumber    номер карты клиента.
     * @param accountNumber номер счёта клиента.
     * @param balance       общий баланс.
     */
    public Recipient(String lastName, String firstName, String patronymic, String cardNumber, Integer accountNumber, Long balance) {
        super(lastName, firstName, patronymic, cardNumber, accountNumber, balance);
    }
}