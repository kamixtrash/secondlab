package model.Transaction;

import model.Client.Recipient;
import model.Client.Sender;
import model.Currency.Currency;

/**
 * Перевод с карты на карту другого пользователя.
 */
public class CardToCardTransaction extends Transaction {
    /**
     * @param amount          сумма перевода.
     * @param currency        валюта, в которой производится перевод.
     * @param recipient       получатель.
     * @param sender          отправитель.
     */
    public CardToCardTransaction(Long amount, Currency currency, Recipient recipient, Sender sender) {
        super(amount, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return "Card to card transaction: " + super.toString();
    }
}