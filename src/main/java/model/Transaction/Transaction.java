package model.Transaction;

import com.google.gson.GsonBuilder;
import model.Client.Client;
import model.Client.Recipient;
import model.Client.Sender;
import model.Currency.Currency;

import java.time.LocalDateTime;

/**
 * Абстрактный класс, представляющий собой перевод.
 */
public abstract class Transaction {
    private final Long id;
    private static Long transactionIdCounter = 0L;
    private final Long amount;
    private final Currency currency;
    private final Recipient recipient;
    private final Sender sender;
    private final LocalDateTime transactionDate;

    public Transaction(Long amount, Currency currency, Recipient recipient, Sender sender) {
        this.id = transactionIdCounter++;
        this.amount = amount;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
        this.transactionDate = LocalDateTime.now();
    }

    public Client getRecipient() {
        return recipient;
    }

    public Client getSender() {
        return sender;
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

    /**
     * Валидация перевода.
     *
     * @return true в том случае, если сумма перевода строго больше нуля и на балансе достаточно средств для перевода.
     */
    public boolean check() {
        return (amount > 0) && (sender.getBalance() >= amount);
    }

    /**
     * Утверждение перевода после успешной валидации или же вывод информации о том, что на балансе недостаточно средств.
     */
    public void commit() {
        if (check()) {
            sender.setBalance(sender.getBalance() - this.amount);
            recipient.setBalance(recipient.getBalance() + this.amount);
        } else {
            System.out.println("Transaction can't be done. Insufficient funds." +
                    " Or you're trying transfer money to yourself.");
        }
    }
}