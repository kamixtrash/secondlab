package model.Transaction;

import model.Client.Recipient;
import model.Client.Sender;
import model.Currency.Currency;

/**
 * Перевод с счёта на счёт другого пользователя.
 */
public class AccountToAccountTransaction extends Transaction {
    /**
     * @param amount          сумма перевода.
     * @param currency        валюта, в которой производится перевод.
     * @param recipient       получатель.
     * @param sender          отправитель.
     */
    public AccountToAccountTransaction(Long amount, Currency currency, Recipient recipient, Sender sender) {
        super(amount, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return "Account to account transaction: " + super.toString();
    }
}